set bg=dark
set showmode
filetype plugin on
set ofu=syntaxcomplete#Complete
fun! MySys()
   return "$1"
endfun
set runtimepath=~/.vim,~/.vim_runtime,~/.vim_runtime/after,\$VIMRUNTIME
source ~/.vim_runtime/vimrc
call pathogen#infect()
helptags ~/.vim_runtime/doc
